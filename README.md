# Chess

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

```

## About
- The project uses Vue.JS as the main frontend library
- The basics of chess have been implemented.
- When you move a player the possible positions are highlighted in red
- Whose turn it is, is shown at the bottom
- You might have to zoom out for the whole thing to render properly
- Instead of a typical 8x8 grid, currently a continuous grid of 64 squares is used. This was done because most of the DnD plugins were working on such lists.   
- The end implementation does NOT use a DnD library. Native HTML/JS DnD is implemented
- Check-mates are not fun. Killing the king is fun. To win the game you actually have to kill the king.
- Most if-else rules have been omitted from this example, as the goal is not to be rules comprehensive but demonstrate the ability to divide the problem into smaller chunks and then conquer it.
- Those rules can, however, be implemented in the Main component with a few if-else statment
- The whole thing has been done without following the React tutorial.
