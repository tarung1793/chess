import Vue from 'vue'
import Router from 'vue-router'
import Mains from '@/components/Mains'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Chess',
      component: Mains
    }
  ]
})
